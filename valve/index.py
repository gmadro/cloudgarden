import requests
import json
import time
import boto3

def main(event, context):
  valve_action = event['action']
  water_valve = event['valve']
  water_time = event['time']
  ssm = boto3.client('ssm')
  username='Gmadro'
  ssmKey=ssm.get_parameter(Name='linktapAPIkey',WithDecryption=True)
  apiKey=ssmKey['Parameter']['Value']
  url='https://www.link-tap.com/api/'

  if valve_action == 'status':
    method='getWateringStatus'
    body = {'username':username, 'apiKey':apiKey, 'taplinkerId':water_valve}
    response = requests.post(url+method, data=body)
    tquery_r = response.json()
    if tquery_r["result"] == 'error':
      status = "LinkTap API Limit. Waiting to retry..."
    else:
      print(tquery_r["status"])
      status = tquery_r["status"]

  elif valve_action == 'water':
    method='getAllDevices'
    body = {'username':username, 'apiKey':apiKey}
    response = requests.post(url+method, data=body)
    aquery_r = response.json()
    gateway_id = aquery_r["devices"][0]['gatewayId']

    method = 'activateInstantMode'
    tl_state = 'true'
    body = {'username':username, 'apiKey':apiKey, 'gatewayId':gateway_id, 'taplinkerId':water_valve, 'action':tl_state, 'duration':water_time}
    response = requests.post(url+method, data=body)
    print (response.text)
    status = response.text

  else:
    status = 'Incorrect action provided'
  return(status)
