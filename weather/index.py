import requests
import json
import time
import boto3

def main(event, context):
    latitude = event['latitude']
    longitude = event['longitude']
    location_url="https://api.weather.gov/points/%s, %s" % (latitude, longitude)
    get_location = requests.get(url=location_url)
    forcast_url = get_location['properties']['forecast']
    get_forecast = requests.get(url=forcast_url)
    return(get_forecast['properties']['periods'])