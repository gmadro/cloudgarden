terraform {
  backend "s3" {
    bucket = "cloudgarden-state"
    region = "us-east-1"
    key    = "tfstate.state"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  default_tags {
    tags = {
      Version = var.ver
    }
  }
}

locals {
  water_dur  = 8 #Time in Minutes
  pump_time  = local.water_dur * 60 - 30
  water_time = 12 #UTC
  lambda_functions = ["pump", "valve", "weather"]
}


data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

data "aws_iam_policy" "sns" {
  name = "AmazonSNSFullAccess"
}

resource "aws_iam_role" "cloudgarden_step" {
  name               = "cloudgarden_step"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.cloudgarden_step_assume.json
}

data "aws_iam_policy_document" "cloudgarden_step_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["states.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "cloudgarden_step_policy" {
  statement {
    actions = [
      "lambda:InvokeFunction"
    ]
    resources = [
      aws_lambda_function.pump.arn,
      aws_lambda_function.valve.arn
    ]
  }
  statement {
    actions = [
      "SNS:Publish"
    ]
    resources = [
      aws_sns_topic.garden_sns_topic.arn
    ]
  }
}

resource "aws_iam_policy" "cloudgarden_step" {
  name   = "cloudgarden_step"
  path   = "/"
  policy = data.aws_iam_policy_document.cloudgarden_step_policy.json
}

resource "aws_iam_role_policy_attachment" "cloudgarden_step" {
  role       = aws_iam_role.cloudgarden_step.name
  policy_arn = aws_iam_policy.cloudgarden_step.arn
}

resource "aws_iam_role" "cloudgarden_lambda" {
  name               = "cloudgarden_lambda"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.cloudgarden_lambda_assume.json
}

data "aws_iam_policy_document" "cloudgarden_lambda_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "cloudgarden_lambda_policy" {
  statement {
    actions = [
      #"ssm:GetParameter",
      "*"
    ]
    resources = [
      #"arn:aws:ssm:${data.aws_region.current.name}:${data.aws_caller_identity.current.id}:parameter:/*",
      "*"
    ]
  }
}

resource "aws_iam_policy" "cloudgarden_lambda" {
  name   = "cloudgarden_lambda"
  path   = "/"
  policy = data.aws_iam_policy_document.cloudgarden_lambda_policy.json
}

resource "aws_iam_role_policy_attachment" "cloudgarden_lambda" {
  role       = aws_iam_role.cloudgarden_lambda.name
  policy_arn = aws_iam_policy.cloudgarden_lambda.arn
}

data "aws_lambda_layer_version" "requests" {
  layer_name = "requests"
}

# resource "aws_lambda_function" "pump" {
#   filename         = "pump.zip"
#   function_name    = "cloud_garden_pump_lambda"
#   role             = aws_iam_role.cloudgarden_lambda.arn
#   handler          = "pump/index.main"
#   source_code_hash = filebase64sha256("pump.zip")
#   runtime          = "python3.9"
#   layers           = [data.aws_lambda_layer_version.requests.arn]
# }

# resource "aws_lambda_function" "valve" {
#   filename         = "valve.zip"
#   function_name    = "cloud_garden_valve_lambda"
#   role             = aws_iam_role.cloudgarden_lambda.arn
#   handler          = "valve/index.main"
#   source_code_hash = filebase64sha256("valve.zip")
#   runtime          = "python3.9"
#   layers           = [data.aws_lambda_layer_version.requests.arn]
# }

resource "aws_lambda_function" "api_calls" {
  for_each = local.lambda_functions
  filename         = "${each.key}.zip"
  function_name    = "cloud_garden_${each.key}_lambda"
  role             = aws_iam_role.cloudgarden_lambda.arn
  handler          = "${each.key}/index.main"
  source_code_hash = filebase64sha256("${each.key}.zip")
  runtime          = "python3.9"
  layers           = [data.aws_lambda_layer_version.requests.arn]
}

resource "aws_sns_topic" "garden_sns_topic" {
  name = "cloud_garden_topic"
}

resource "aws_sns_topic_subscription" "garden_sns_sub" {
  topic_arn = aws_sns_topic.garden_sns_topic.arn
  protocol  = "email"
  endpoint  = "greg.madro@gmail.com"
}

resource "aws_sfn_state_machine" "cloud_garden" {
  name     = "cloud-garden"
  role_arn = aws_iam_role.cloudgarden_step.arn

  definition = <<EOF
  {
    "Comment": "CloudGarden ${var.ver}",
    "StartAt": "OpenValves1",
    "States": {
      "OpenValves1": {
        "Type": "Task",
        "Resource": "arn:aws:states:::lambda:invoke",
        "Parameters": {
            "FunctionName":"${aws_lambda_function.valve.arn}",
            "Payload": {
              "action": "water",
              "valve": "DC5CBE23004B1200",
              "time": "${local.water_dur}"
            }
         },
        "Next": "OpenValves2",
        "Catch": [
          {
            "ErrorEquals": [
              "States.ALL"
            ],
            "Next": "SNS_FAIL"
          }
        ]
      },
      "OpenValves2": {
        "Type": "Task",
        "Resource": "arn:aws:states:::lambda:invoke",
        "Parameters": {
            "FunctionName":"${aws_lambda_function.valve.arn}",
            "Payload": {
              "action": "water",
              "valve": "F7A3BE23004B1200",
              "time": "${local.water_dur}"
            }
         },
        "Next": "WaitForValves",
        "Catch": [
          {
            "ErrorEquals": [
              "States.ALL"
            ],
            "Next": "SNS_FAIL"
          }
        ]
      },
      "WaitForValves": {
        "Type": "Wait",
        "Seconds": 30,
        "Next": "TurnOnPump"
      },
      "TurnOnPump": {
        "Type": "Task",
        "Resource": "arn:aws:states:::lambda:invoke",
        "Parameters": {
            "FunctionName":"${aws_lambda_function.pump.arn}",
            "Payload": {
              "action": "on"
            }
         },
        "Next": "WaitForPump",
        "Catch": [
          {
            "ErrorEquals": [
              "States.ALL"
            ],
            "Next": "SNS_FAIL"
          }
        ]
      },
      "WaitForPump": {
        "Type": "Wait",
        "Seconds": ${local.pump_time},
        "Next": "TurnOffPump"
      },
      "TurnOffPump": {
        "Type": "Task",
        "Resource": "arn:aws:states:::lambda:invoke",
        "Parameters": {
            "FunctionName":"${aws_lambda_function.pump.arn}",
            "Payload": {
              "action": "off"
            }
         },
        "Next": "SNS_PASS",
        "Catch": [
          {
            "ErrorEquals": [
              "States.ALL"
            ],
            "Next": "SNS_FAIL"
          }
        ]
      },
      "SNS_PASS": {
        "Type": "Task",
        "Resource": "arn:aws:states:::sns:publish",
        "Parameters": {
          "TopicArn": "${aws_sns_topic.garden_sns_topic.arn}",
          "Message": {
            "Input": "Play Passed"
          }
        },
        "End": true
      },
      "SNS_FAIL": {
        "Type": "Task",
        "Resource": "arn:aws:states:::sns:publish",
        "Parameters": {
          "TopicArn": "${aws_sns_topic.garden_sns_topic.arn}",
          "Message": {
            "Input": "Play Failed"
          }
        },
        "End": true
      }
    }
  }
EOF
}

resource "aws_cloudwatch_event_rule" "water" {
  name                = "water-garden"
  description         = "Start watering the garden"
  schedule_expression = "cron(0 ${local.water_time} * * ? *)"
}

resource "aws_cloudwatch_event_target" "cloud_garden_step" {
  rule      = aws_cloudwatch_event_rule.water.name
  target_id = "StartCloudGarden"
  arn       = aws_sfn_state_machine.cloud_garden.arn
  role_arn  = aws_iam_role.cloudgarden_event.arn
}

resource "aws_iam_role" "cloudgarden_event" {
  name               = "cloudgarden_event"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.cloudgarden_event_assume.json
}

data "aws_iam_policy_document" "cloudgarden_event_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "cloudgarden_event_policy" {
  statement {
    actions = [
      "states:StartExecution"
    ]
    resources = [
      aws_sfn_state_machine.cloud_garden.arn
    ]
  }
}

resource "aws_iam_policy" "cloudgarden_event" {
  name   = "cloudgarden_event"
  path   = "/"
  policy = data.aws_iam_policy_document.cloudgarden_event_policy.json
}

resource "aws_iam_role_policy_attachment" "cloudgarden_event" {
  role       = aws_iam_role.cloudgarden_event.name
  policy_arn = aws_iam_policy.cloudgarden_event.arn
}