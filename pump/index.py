import requests
import json
import time
import boto3

def main(event, context):
    pump_action = event['action']
    ssm = boto3.client('ssm')
    username = 'greg.madro@gmail.com'
    ssmKey = ssm.get_parameter(Name='kasaPW',WithDecryption=True)
    kasaPW = ssmKey['Parameter']['Value']
    url = 'https://wap.tplinkcloud.com'
    uuid = '0990261b-7e11-4674-be06-e9a9c0b632c8'
    method = 'login'

    body = "{ 'method': %s, 'params': { 'appType': 'Kasa_Android', 'cloudUserName': %s, 'cloudPassword': %s, 'terminalUUID': %s } }" % (method, username, kasaPW, uuid)

    get_token = requests.post(url=url, data=body)
    kasa_r = get_token.json()
    token = kasa_r['result']['token']

    body = "{'method':'getDeviceList'}"

    query = requests.post(url=url+'?token='+token, data=body)
    devices = query.json()
    device_id = devices['result']['deviceList'][2]['deviceId']
    control_url= devices['result']['deviceList'][2]['appServerUrl']

    body = "{'method':'passthrough', 'params': {'deviceId': %s, 'requestData' : { 'system' : { 'get_sysinfo' : null } } } }" % (device_id)
    d_query = requests.post(url=control_url+'?token='+token, data=body)

    plugs = d_query.json()
    rainpump = plugs['result']['responseData']['system']['get_sysinfo']['children'][1]['id']

    if pump_action == 'on':
        plug_state = 1
    else:
        plug_state = 0

    body = "{'method': 'passthrough',	'params': { 'deviceId': %s, 'requestData': { 'context': { 'child_ids': [ %s ] }, 'system': { 'set_relay_state': { 'state': %s } } }, 'token': %s } }" % (device_id, rainpump, plug_state, token)
    execute = requests.post(url=control_url, data=body)
    return(execute.text)